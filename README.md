View this project on [CADLAB.io](https://cadlab.io/project/1775). 

# High Power Rocketry Telemetry Module

A telemtry module for high power rocketry missions, based around STM32L1.

## Documentation


## Repository policy
TBD

## License

&copy; 2017 Libre Space Foundation & commiters

Licensed under the [CERN OHLv1.2](LICENSE).
