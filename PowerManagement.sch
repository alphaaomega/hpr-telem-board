EESchema Schematic File Version 2
LIBS:hpr-telem-board-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:w_analog
LIBS:sprk_rfm69hcw
LIBS:gy-gps6mv2
LIBS:ESP8266
LIBS:bme280
LIBS:si1401edh
LIBS:hpr-telem-board-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X02 P1
U 1 1 58C1F1E4
P 3050 2800
F 0 "P1" H 3050 2950 50  0000 C CNN
F 1 "LiPo Battery" V 3150 2800 50  0000 C CNN
F 2 "Connectors_JST:JST_EH_S02B-EH_02x2.50mm_Angled" H 3050 2800 50  0001 C CNN
F 3 "" H 3050 2800 50  0000 C CNN
	1    3050 2800
	-1   0    0    -1  
$EndComp
$Comp
L LD1117S33TR U6
U 1 1 58C21462
P 3850 2800
F 0 "U6" H 3850 3200 50  0000 C CNN
F 1 "LD1117S33TR" H 3850 3100 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-223" H 4300 3000 50  0000 C CNN
F 3 "" H 3850 2800 50  0000 C CNN
	1    3850 2800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR020
U 1 1 58C21470
P 3850 3250
F 0 "#PWR020" H 3850 3000 50  0001 C CNN
F 1 "GND" H 3850 3100 50  0000 C CNN
F 2 "" H 3850 3250 50  0000 C CNN
F 3 "" H 3850 3250 50  0000 C CNN
	1    3850 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 2850 3400 2850
Wire Wire Line
	3400 2850 3400 3150
Wire Wire Line
	3400 3150 4400 3150
Wire Wire Line
	3850 3050 3850 3250
Connection ~ 3850 3150
Text HLabel 5050 2750 2    60   Input ~ 0
VDD
Text HLabel 4100 3350 3    60   Input ~ 0
GND
Wire Wire Line
	4250 2750 5050 2750
$Comp
L PWR_FLAG #FLG021
U 1 1 58C28CE1
P 3400 2750
F 0 "#FLG021" H 3400 2845 50  0001 C CNN
F 1 "PWR_FLAG" V 3400 3050 50  0000 C CNN
F 2 "" H 3400 2750 50  0000 C CNN
F 3 "" H 3400 2750 50  0000 C CNN
	1    3400 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 2750 3450 2750
Connection ~ 3400 2750
$Comp
L R R15
U 1 1 58C83FA6
P 3900 1450
F 0 "R15" V 3980 1450 50  0000 C CNN
F 1 "47K" V 3800 1450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 3830 1450 50  0001 C CNN
F 3 "" H 3900 1450 50  0001 C CNN
	1    3900 1450
	1    0    0    -1  
$EndComp
$Comp
L R R16
U 1 1 58C83FDB
P 3900 1850
F 0 "R16" V 3980 1850 50  0000 C CNN
F 1 "22K" V 3800 1850 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 3830 1850 50  0001 C CNN
F 3 "" H 3900 1850 50  0001 C CNN
	1    3900 1850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR022
U 1 1 58C84020
P 3900 2000
F 0 "#PWR022" H 3900 1750 50  0001 C CNN
F 1 "GND" H 3900 1850 50  0000 C CNN
F 2 "" H 3900 2000 50  0001 C CNN
F 3 "" H 3900 2000 50  0001 C CNN
	1    3900 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 2750 3250 1300
Wire Wire Line
	3250 1300 3900 1300
Wire Wire Line
	3900 1600 3900 1700
Text HLabel 4150 1700 2    60   Input ~ 0
BATSENSE
Wire Wire Line
	3900 1700 4150 1700
Wire Wire Line
	4900 2750 4900 3250
Connection ~ 4900 2750
Connection ~ 4900 3050
Wire Wire Line
	4100 3150 4100 3350
Connection ~ 4100 3150
Wire Wire Line
	4400 3050 4400 3250
Connection ~ 4400 3150
$Comp
L CONN_02X03 P8
U 1 1 58CAED83
P 4650 3150
F 0 "P8" H 4650 3350 50  0000 C CNN
F 1 "CONN_02X03" H 4650 2950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03" H 4650 1950 50  0001 C CNN
F 3 "" H 4650 1950 50  0000 C CNN
	1    4650 3150
	1    0    0    -1  
$EndComp
Connection ~ 4900 3150
$EndSCHEMATC
