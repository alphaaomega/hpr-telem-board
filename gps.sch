EESchema Schematic File Version 2
LIBS:hpr-telem-board-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:w_analog
LIBS:sprk_rfm69hcw
LIBS:gy-gps6mv2
LIBS:ESP8266
LIBS:bme280
LIBS:si1401edh
LIBS:hpr-telem-board-cache
EELAYER 25 0
EELAYER END
$Descr User 7874 4000
encoding utf-8
Sheet 5 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GY-GPS6MV2 U4
U 1 1 58BF4442
P 1900 1500
AR Path="/58BF43D1/58BF4442" Ref="U4"  Part="1" 
AR Path="/58BF43D1/58BF43D1/58BF4442" Ref="U4"  Part="1" 
F 0 "U4" H 1900 1500 60  0000 C CNN
F 1 "GY-GPS6MV2" H 1900 1350 60  0000 C CNN
F 2 "libs:GY-NEO6MV2" H 1900 1500 60  0001 C CNN
F 3 "" H 1900 1500 60  0001 C CNN
	1    1900 1500
	1    0    0    -1  
$EndComp
Text HLabel 1300 2900 3    60   Input ~ 0
VCC
Text HLabel 2200 2550 3    60   Input ~ 0
GND
Text HLabel 1800 2550 3    60   Input ~ 0
GPS_RX
Text HLabel 2000 2550 3    60   Output ~ 0
GPS_TX
Wire Wire Line
	1600 2200 1600 2300
Wire Wire Line
	1800 2550 1800 2200
Wire Wire Line
	2000 2550 2000 2200
Wire Wire Line
	2200 2550 2200 2200
$Comp
L Si1401EDH Q3
U 1 1 58C8198D
P 1200 2500
F 0 "Q3" H 1500 2550 50  0000 R CNN
F 1 "Si1401EDH" H 1825 2450 50  0000 R CNN
F 2 "TO_SOT_Packages_SMD:SC-70-6_Handsoldering" H 1400 2600 50  0001 C CNN
F 3 "" H 1200 2500 50  0000 C CNN
	1    1200 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 2300 1300 2250
Wire Wire Line
	1300 2250 1600 2250
Connection ~ 1600 2250
Wire Wire Line
	1400 2250 1400 2300
Connection ~ 1400 2250
Wire Wire Line
	1500 2250 1500 2300
Connection ~ 1500 2250
$Comp
L R R12
U 1 1 58C81A84
P 1150 2750
F 0 "R12" V 1230 2750 50  0000 C CNN
F 1 "100k" V 1150 2750 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 1080 2750 50  0001 C CNN
F 3 "" H 1150 2750 50  0000 C CNN
	1    1150 2750
	0    1    1    0   
$EndComp
Text HLabel 1000 2900 3    60   Input ~ 0
GPS_EN
Wire Wire Line
	1000 2500 1000 2900
Connection ~ 1000 2750
Wire Wire Line
	1300 2700 1300 2900
Connection ~ 1300 2750
$Comp
L PWR_FLAG #FLG015
U 1 1 58C8D730
P 1150 2200
F 0 "#FLG015" H 1150 2295 50  0001 C CNN
F 1 "PWR_FLAG" H 1150 2380 50  0000 C CNN
F 2 "" H 1150 2200 50  0000 C CNN
F 3 "" H 1150 2200 50  0000 C CNN
	1    1150 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 2200 1600 2200
$Comp
L CONN_01X02 P6
U 1 1 58CAD0F9
P 2650 2300
F 0 "P6" H 2650 2450 50  0000 C CNN
F 1 "CONN_01X02" V 2750 2300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 2650 2300 50  0001 C CNN
F 3 "" H 2650 2300 50  0000 C CNN
	1    2650 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 2250 2450 2250
Connection ~ 1800 2250
Wire Wire Line
	2000 2350 2450 2350
Connection ~ 2000 2350
$Comp
L CONN_01X04 P10
U 1 1 58CC51C6
P 4200 1550
F 0 "P10" H 4200 1800 50  0000 C CNN
F 1 "GPS_ANT" V 4300 1550 50  0000 C CNN
F 2 "libs:GY-NEO6MV2-ANT" H 4200 1550 50  0001 C CNN
F 3 "" H 4200 1550 50  0000 C CNN
	1    4200 1550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 58CC5223
P 3850 1800
F 0 "#PWR016" H 3850 1550 50  0001 C CNN
F 1 "GND" H 3850 1650 50  0000 C CNN
F 2 "" H 3850 1800 50  0000 C CNN
F 3 "" H 3850 1800 50  0000 C CNN
	1    3850 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 1800 3850 1400
Wire Wire Line
	3850 1400 4000 1400
Wire Wire Line
	3850 1500 4000 1500
Connection ~ 3850 1500
Wire Wire Line
	3850 1600 4000 1600
Connection ~ 3850 1600
Wire Wire Line
	3850 1700 4000 1700
Connection ~ 3850 1700
$EndSCHEMATC
